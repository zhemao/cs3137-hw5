import javax.swing.JComponent;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Color;

public class MapPanel extends JComponent {
	City cities[];
	Edge edges[];
	int div = 5;
	int margin = 10;

	public MapPanel(City[] cities, Edge[] edges){
		setPreferredSize(new Dimension(600, 325));
		setOpaque(true);
		this.cities = cities;
		this.edges = edges;
	}

	public void drawCities(Graphics g){
		int height = getSize().height;
		g.setColor(Color.RED);

		for(City c: cities){
			int x = c.getX() / div + margin;
			int y = height - c.getY()/div - margin;
			g.drawOval(x-1, y-1, 2, 2);
			g.drawString(c.getName(), x, y);
		}
	}

	public void drawEdges(Graphics g){
		int height = getSize().height;
		g.setColor(Color.BLACK);

		for(Edge e: edges){
			City a = e.getStart();
			City b = e.getEnd();

			int ax = a.getX() / div + margin;
			int ay = height - a.getY() / div - margin;

			int bx = b.getX() / div + margin;
			int by = height - b.getY() / div - margin;

			g.drawLine(ax, ay, bx, by);
		}
	}

	public void paintComponent(Graphics g){
		drawCities(g);
		drawEdges(g);
	}
}
