import java.util.LinkedList;
import java.util.Scanner;
import java.util.PriorityQueue;

import java.io.IOException;
import java.io.FileInputStream;

import javax.swing.JFrame;

public class MSTFinder {
	/**
	 * Read a LinkedList of cities in from the city file
	 */
	public static City[] readCities(String filename){
		LinkedList<City> cities = new LinkedList<City>();
		Scanner input;
		// the city id will just be the city's order in the file
		int id = 0;

		try {
			input = new Scanner(new FileInputStream(filename));
		} catch (Exception e) { e.printStackTrace(); return null; }

		while(input.hasNextLine()){
			String line = input.nextLine();
			if(line.length() > 0){
				String[] row = line.split(" ");
				int x = Integer.parseInt(row[1]);
				int y = Integer.parseInt(row[2]);
				City city = new City(id, row[0], x, y);
				cities.add(city);
				id++;
			}
		}

		City[] cityLinked = new City[cities.size()];

		return cities.toArray(cityLinked);
	}

	/**
	 * Generate a complete graph for the cities
	 */
	public static LinkedList<Edge> completeGraph(City[] cities){
		LinkedList<Edge> edges = new LinkedList<Edge>();

		for(int i=0; i<cities.length-1; i++){
			for(int j=i+1; j<cities.length; j++){
				edges.add(new Edge(cities[i], cities[j]));
			}
		}

		return edges;
	}

	/**
	 * Find a minimal spanning tree for cities. 
	 * @param alledges The complete graph for the cities
	 * @param n The number of cities
	 */
	public static Edge[] findMST(LinkedList<Edge> alledges, int n){
		UnionFindTree uftree = new UnionFindTree(n);
		PriorityQueue<Edge> edgeq = new PriorityQueue<Edge>(alledges);
		Edge[] mst = new Edge[n-1];
		int i = 0;

		while(!edgeq.isEmpty()){
			Edge edge = edgeq.poll();
			int a = edge.getStartID();
			int b = edge.getEndID();

			if(!uftree.sameSet(a, b)){
				mst[i] = edge;
				i++;
				uftree.union(a, b);
			}
		}

		return mst;
	}

	public static void main(String args[]){
		if(args.length == 0){
			System.out.println("Usage: java MSTFinder filename");
			System.exit(1);
		}
		City[] cities = readCities(args[0]);
		LinkedList<Edge> edges = completeGraph(cities);

		System.out.println("Complete graph");
		for(Edge e: edges){
			System.out.println(e.toString());
		}

		Edge[] mst = findMST(edges, cities.length);
		System.out.println("\nMinimal Spanning Tree");
		for(Edge e: mst){
			System.out.println(e.toString());
		}

		JFrame frame = new JFrame("Minimal Spanning Tree");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MapPanel map = new MapPanel(cities, mst);
		frame.setContentPane(map);
		frame.pack();
		frame.setVisible(true);
	}
}
