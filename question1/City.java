/**
 * Represents a city on the map
 * Contains the name, x-y coordinates, and a unique id 
 */

public class City {
	private String name;
	private int x, y;
	private int id;

	public City(int id, String name, int x, int y){
		this.id = id;
		this.name = name;
		this.x = x;
		this.y = y;
	}

	public int getID(){
		return id;
	}

	public int getX(){
		return x;
	}

	public int getY(){
		return y;
	}

	public String getName(){
		return name;
	}
}
