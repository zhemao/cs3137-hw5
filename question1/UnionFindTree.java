/**
 * Simple implementation of a union-find tree
 */
public class UnionFindTree {
	private int[] parents;

	public UnionFindTree(int n){
		parents = new int[n];
		for(int i=0; i<n; i++){
			parents[i] = i;
		}
	}

	/**
	 * find the root of the set to which i belongs
	 */
	public int find(int i){
		while(parents[i] != i){
			i = parents[i];
		}
		return i;
	}

	/**
	 * returns true if i and j are in the same set, false otherwise
	 */
	public boolean sameSet(int i, int j){
		return find(i) == find(j);
	}

	/**
	 * union operation to put i and j in the same set
	 */
	public void union(int i, int j){
		int pi = find(i);
		int pj = find(j);

		if(pi != pj){
			parents[pi] = pj;
		}
	}
}
