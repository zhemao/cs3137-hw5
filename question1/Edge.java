/**
 * Represents an edge connecting two cities
 */

public class Edge implements Comparable<Edge>{
	private double distance;
	private City start, end;

	public Edge(City start, City end){
		this.start = start;
		this.end = end;

		int dx = start.getX() - end.getX();
		int dy = start.getY() - end.getY();

		distance = Math.sqrt(dx*dx + dy*dy);
	}

	public City getStart(){
		return start;
	}

	public City getEnd(){
		return end;
	}

	public int getStartID(){
		return start.getID();
	}

	public int getEndID(){
		return end.getID();
	}

	public double getDistance(){
		return distance;
	}

	public String toString(){
		String str = start.getName() + " - " + end.getName();
		str += " " + distance;
		return str;
	}

	// compare edges by distance from start to end
	public int compareTo(Edge other){
		return (int)(distance - other.distance);
	}
}
