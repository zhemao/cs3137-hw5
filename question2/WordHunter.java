import java.io.FileInputStream;
import java.io.IOException;

import java.util.Scanner;

public class WordHunter {
	/**
	 * Read in the WordHunt puzzle from the file
	 */
	public static char[][][] readGrid(String filename){
		char[][][] grid = new char[3][3][2];
		Scanner input = null;
		char curChar = 0;

		try{
			input = new Scanner(new FileInputStream(filename));
		} catch(Exception e){ e.printStackTrace(); return grid; }

		while(input.hasNextLine()){
			String line = input.nextLine();
			if(line.length() == 0)
				continue;
			if(curChar==0)
				curChar = line.charAt(0);
			else {
				String[] numbers = line.split(" ");
				
				int x = Integer.parseInt(numbers[0]);
				int y = Integer.parseInt(numbers[1]);
				int z = Integer.parseInt(numbers[2]);

				grid[x][y][z] = curChar;

				curChar = 0;
			}
		}

		return grid;
	}

	/**
	 * Read in the dictionary file
	 */
	public static Dictionary readDictionary(String filename, int n){
		Dictionary dict = new Dictionary(n);
		Scanner input = null;

		try{
			input = new Scanner(new FileInputStream(filename));
		} catch(Exception e){ e.printStackTrace(); return dict; }

		while(input.hasNextLine()){
			dict.addWord(input.nextLine());
		}

		return dict;
	}

	/**
	 * Find all words in the WordHunt grid
	 * @param str The string that has been built thus far
	 * @param x The x position of the current letter
	 * @param y The y position of the current letter
	 * @param z The z position of the current letter
	 * @param n The number of letters left to check
	 * @param grid The word hunt grid
	 * @param dict The word dictionary
	 */
	public static void findWords(String str, int x, int y, int z, int n, 
							char[][][] grid, Dictionary dict){
		str += grid[x][y][z];
		if(n == 1){
			if(dict.contains(str)){
				System.out.println(str);
			}
		} else {
			if(x < grid.length-1)
				findWords(str, x+1, y, z, n-1, grid, dict);
			if(x > 0)
				findWords(str, x-1, y, z, n-1, grid, dict);
			if(y < grid[0].length-1)
				findWords(str, x, y+1, z, n-1, grid, dict);
			if(y > 0)
				findWords(str, x, y-1, z, n-1, grid, dict);
			if(z < grid[0][0].length-1)
				findWords(str, x, y, z+1, n-1, grid, dict);
			if(z > 0)
				findWords(str, x, y, z-1, n-1, grid, dict);
		}
	}

	public static void main(String args[]){
		if(args.length < 3){
			System.out.println("Usage: java WordHunter n wordgrid dictionary");
			System.exit(1);
		}
		
		int n = Integer.parseInt(args[0]);
		char[][][] grid = readGrid(args[1]);
		Dictionary dict = readDictionary(args[2], 304961);

		for(int i=0; i<grid.length; i++)
			for(int j=0; j<grid[i].length; j++)
				for(int k=0; k<grid[i][j].length; k++)
					findWords("", i, j, k, n, grid, dict);
	}

}
