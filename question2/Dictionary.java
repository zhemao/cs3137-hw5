/**
 * A simple hash set implementation with linear probing
 */
public class Dictionary {
	String table[];
	
	public Dictionary(int n){
		table = new String[n];
	}

	public int hash(String str){
		int h = 0;

		for(int i=0; i<str.length(); i++){
			h = 37 * (h + str.charAt(i));
		}

		h = h % table.length;
		if(h < 0) return h + table.length;
		return h;
	}

	public void addWord(String word){
		int h = hash(word);

		for(; h < table.length; h++){
			if(table[h] == null){
				table[h] = word;
				break;
			}
			if(table[h].equals(word))
				break;
		}
	}

	public boolean contains(String word){
		int h = hash(word);

		for(; h < table.length; h++){
			if(table[h] == null)
				return false;
			else if(table[h].equals(word))
				return true;
		}

		return false;
	}
}
