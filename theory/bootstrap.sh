#!/bin/sh

DOWNLOADS="q1graph.png q4dfstree.png q5components.png"

user=zhemao
read pass

for file in $DOWNLOADS
do
	if [ ! -f $file ]; then
		echo "Downloading $file"
		curl -i --anyauth --user $user:$pass -o $file \
			https://bitbucket.org/zhemao/cs3137-hw5/downloads/$file
		if [ $? == "0" ]; then
			echo "Downloaded $file"
		fi
	fi
done
